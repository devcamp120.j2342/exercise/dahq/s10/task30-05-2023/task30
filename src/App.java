import model.Customer;
import model.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer(1, "customer1", 10);
        Invoice invoice1 = new Invoice();
        Invoice invoice2 = new Invoice(2, customer2, 5.8);
        System.out.println(customer2.toString());
        System.out.println(invoice2.toString());
    }
}
